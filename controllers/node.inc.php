<?php
  header('Access-Control-Allow-Origin: *');
  header('Content-Type: text/json; charset="UTF-8"');

  # GET
  if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    $error = '';
    $nodes = [];
    $params = [
      'q' => _get('search_keyword'),
      'limit' => _get('page_size', DEFAULT_PAGE_SIZE),
      'offset' => _get('page_num', 0),
      'pretty' => _get('pretty'),
    ];

    # check language
    if(!in_array($language, $allowed_langs)) {
      $error = 'Invalid language. Possible values: "' . implode('", "', $allowed_langs) . '".';

    # check valid page_size
  } elseif(!ctype_digit((string)$params['limit']) || $params['limit'] < MIN_PAGE_SIZE || $params['limit'] > MAX_PAGE_SIZE) {
      $error = 'Invalid page size requested';

    # check valid page_num
    } elseif(!ctype_digit((string)$params['offset'])) {
        $error = 'Invalid page number requested';

    # check valid idNode
    } elseif(!$idNode) {
      $error = 'Invalid node id';

    } else {
      $params['limit'] = (int)$params['limit'];
      $params['offset'] = (int)$params['offset'];
      $node = models\NestedSetNode::get_by_id($idNode);
      # check valid Node element
      if(!$node) {
        $error = 'Invalid node id';
      } else {
        $nodesCount = models\NestedSet::getNodesCount($node, $language, $params);
        $params['offset'] = $params['limit'] * $params['offset'];
        # check valid page_num
        if($params['offset'] < 0 || $params['offset'] > $nodesCount) {
          $error = 'Invalid page number requested';
        } else {
          # no errors
          $nodes = models\NestedSet::getNodes($node, $language, $params, $params['offset'], $params['limit']);
        }
      }
    }

    $result = [
      'nodes' => $nodes,
      'error' => $error,
    ];

    echo json_encode($result, $params['pretty'] ? JSON_PRETTY_PRINT : 0);
  }

  # DEFAULT
  else {
    header('HTTP/1.0 400 Bad Request', true, 400);
    header("Content-Type: application/json; charset=utf-8");
    echo json_encode(array("error" => "unsupported method"));
  }

?>
