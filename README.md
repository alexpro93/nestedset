# Nested Set

## Installation
Copy the folder into a web-server, create a database with the SQL file `sql/schema_evolution_00000000.sql` and configure the constants into the file `includes/config.inc.php`.
### Configuration constants
- DEBUG: (boolead): if true it display errors.
- DB_HOST: (string): Database host
- DB_DATABASE: (string): Database name
- DB_USER: (string): Database user
- DB_PASSWORD: (string): Database password
- DB_CHARSET: (string): Database charset
- BASE_URL: (string): Web path to index.php, with domain and subfolders, without final slash
- EXTRA_PATH: (integer): URL parts after domain, used if the project is in a subfolder of domain

### Endpoint configurations
- MIN_PAGE_SIZE: (integer): the minimun size of the page to retrieve
- DEFAULT_PAGE_SIZE: (integer): the default size of the page to retrieve, if not provided
- MAX_PAGE_SIZE: (integer): the maximum size of the page to retrieve
- $allowed_langs: (array of strings): Allowed language identifier.

## Endpoint
### Request
The API is reachable with GET request at `/api/node/{:language}/{:idNode}`
- idNode (integer, required) : the unique ID of the selected node.
- language (enum, required) : language identifier. Possible values: "english", "italian".

With the following optional parameters:
- search_keyword: (string) : a search term used to filter results. If provided, restricts the results to "all children nodes under node_id whose nodeName in the given language contains search_keyword (case insensitive)".
- page_num (integer) : the 0-based identifier of the page to retrieve. If not provided, defaults to “0”.
- page_size (integer) : the size of the page to retrieve, ranging from 0 to 1000. If not provided, defaults to “100”.
- pretty (boolean) : default 0, if provided it print the json in the pretty format.


### Return values
The API return a JSON with the following fields:
- nodes (array, required) : 0 or more nodes matching the given conditions. Each node contains:
  - node_id (integer, required) : the unique ID of the child node.
  - name (string, required) : the node name translated in the requested language.
  - children_count (integer, required) : the number of child nodes of this node.
- error (string, optional) : If there was an error, return the generated message.
