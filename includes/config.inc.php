<?php
  define('DEBUG', false);

  define('DB_HOST', 'localhost');
  define('DB_DATABASE', 'nested_set');
  define('DB_USER', 'root');
  define('DB_PASSWORD', 'root');
  define('DB_CHARSET', 'utf8');

  define('BASE_URL', 'http://localhost/PromoQui_NestedSet');
  define('EXTRA_PATH', 1); // parts after domain name to be removed by BASE_URL

  define('MIN_PAGE_SIZE', 0);
  define('DEFAULT_PAGE_SIZE', 100);
  define('MAX_PAGE_SIZE', 1000);

  if (DEBUG) {
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
  }

  $allowed_langs = [
    'italian',
    'english'
  ];

?>
