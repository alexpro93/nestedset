<?php

function _get($key, $default = null) {
  global $_GET;
  return isset($_GET[$key]) ? $_GET[$key] : $default;
}

?>
