<?php

  # PDO database connection
  try {
    $pdo = new PDO(
        "mysql:host=".DB_HOST.";dbname=".DB_DATABASE.";charset=".DB_CHARSET,
        DB_USER,
        DB_PASSWORD
    );
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  } catch(PDOException $e) {
    error_log($e->getMessage());
    die("A database error was encountered -> " . $e->getMessage() );
  }

?>
