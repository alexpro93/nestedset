<?php
  require_once("includes/loader.inc.php");

  $request_uri = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
  $request_parts = explode("/", trim($request_uri, "/"));
  $request_parts = array_slice($request_parts, EXTRA_PATH);
  $request_uri = "/" . implode("/", $request_parts);

  if (count($request_parts) == 0 ||
      count($request_parts) == 1 && $request_parts[0] == '') {

      header("Content-Type: application/json; charset=utf-8");
      echo json_encode(array("info" => "call /api/node/{:language}/{:idNode}"));
  }

  # /api
  elseif (count($request_parts) >= 1 && $request_parts[0] == 'api') {
    $request_parts = array_slice($request_parts, 1);

    # preflight
    if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
      if (isset($_SERVER['HTTP_ORIGIN']) && $_SERVER['HTTP_ORIGIN']) {
        header("Access-Control-Allow-Origin: " . $_SERVER['HTTP_ORIGIN']);
        header("Access-Control-Allow-Methods: HEAD, GET, POST, PUT, DELETE, OPTIONS");
        header("Access-Control-Allow-Headers: Authorization, Content-Type");
      }
      echo "Allow: HEAD, GET, POST, PUT, DELETE, OPTIONS";
      exit;
    }

    # /node/{:language}/{:idNode}
    if (count($request_parts) == 3 && $request_parts[0] == 'node' && ctype_digit($request_parts[2])) {
      $language = $request_parts[1];
      $idNode = $request_parts[2];
      require("controllers/node.inc.php"); exit;
    } elseif(count($request_parts) >= 1 && $request_parts[0] == 'node') {
      header("Content-Type: application/json; charset=utf-8");
      echo json_encode([
        'nodes' => [],
        'error' => 'Missing mandatory params',
      ]);
    } else {
      header('HTTP/1.0 400 Bad Request', true, 400);
      header("Content-Type: application/json; charset=utf-8");
      echo json_encode(array("error" => "unsupported method, no api found"));
    }
  } else {
    header('HTTP/1.0 400 Bad Request', true, 400);
    header("Content-Type: application/json; charset=utf-8");
    echo json_encode(array("error" => "unsupported method, no endpoint found"));
  }

?>
