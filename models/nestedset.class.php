<?php
  namespace models;
  use PDO;

  class NestedSetNode extends \ArrayObject {
    #get_by_id
    static public function get_by_id($id) {
      global $pdo;
      $query = "SELECT *
        FROM
            node_tree
        WHERE
        	idNode = :id;";
      $stmt = $pdo->prepare($query);
      $stmt->bindParam(':id', $id);
      $stmt->execute();

      $r = $stmt->fetch(PDO::FETCH_ASSOC);
      if ($r) {
        return new NestedSetNode($r);
      }
      return null;
    }

    #construct
    function __construct($value) {
      parent::__construct($value);

      $this['idNode'] = (int)$this['idNode'];
      $this['level'] = (int)$this['level'];
      $this['iLeft'] = (int)$this['iLeft'];
      $this['iRight'] = (int)$this['iRight'];
    }
  }

  class NestedSet {
    static public function getWhere($params = array()) {
      global $pdo;
      $where = "";
      if (isset($params['q']) && $params['q']) {
        $where .= " AND (LOWER(node_tree_names.nodeName) like LOWER(" . $pdo->quote('%' . $params['q'] . '%') . "))";
      }

      return $where;
    }

    #getNodesCount
    static public function getNodesCount($node, $params = array()) {
      global $pdo;
      $where = NestedSet::getWhere($params);
      $query = "SELECT COUNT(*) AS count
        FROM
            node_tree AS Child
        WHERE
            Child.level = :level + 1
                AND Child.iLeft > :iLeft
                AND Child.iRight < :iRight
            $where;";
      $stmt = $pdo->prepare($query);
      $stmt->bindValue(':level', $node['level']);
      $stmt->bindValue(':iLeft', $node['iLeft']);
      $stmt->bindValue(':iRight', $node['iRight']);
      $stmt->execute();
      $r = $stmt->fetch(PDO::FETCH_ASSOC);
      return (int)$r['count'];
    }

    #getNodes
    static public function getNodes($node, $language, $params = array(), $offset = 0, $limit = DEFAULT_PAGE_SIZE) {
      global $pdo;
      $where = NestedSet::getWhere($params);
      $query = "SELECT
          Child.idNode, node_tree_names.nodeName, COUNT(Children.idNode) AS children_count
        FROM
            node_tree AS Child
            INNER JOIN
            node_tree_names ON Child.idNode = node_tree_names.idNode
            LEFT JOIN
            node_tree AS Children
            ON Children.level = Child.level + 1
            AND Children.iLeft > Child.iLeft
                AND Children.iRight < Child.iRight
        WHERE
          node_tree_names.language = :language AND
            Child.level = :level + 1
                AND Child.iLeft > :iLeft
                AND Child.iRight < :iRight
            $where

        GROUP BY Child.idNode, node_tree_names.nodeName
        LIMIT :limit OFFSET :offset;";
      $stmt = $pdo->prepare($query);
      $stmt->bindValue(':language', $language);
      $stmt->bindValue(':level', $node['level']);
      $stmt->bindValue(':iLeft', $node['iLeft']);
      $stmt->bindValue(':iRight', $node['iRight']);
      $stmt->bindValue(':limit', $limit, PDO::PARAM_INT);
      $stmt->bindValue(':offset', $offset, PDO::PARAM_INT);
      $stmt->execute();
      $results = array();
      while ($r = $stmt->fetch(PDO::FETCH_ASSOC)) {
        $results[] = array(
          'node_id' => (int)$r['idNode'],
          'name' => $r['nodeName'],
          'children_count' => (int)$r['children_count'],
        );
      }
      return $results;
    }

  }


?>
